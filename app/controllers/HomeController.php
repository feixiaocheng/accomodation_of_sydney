<?php

class HomeController extends Controller
{
    private $area;
    private $region;

    /**
     * Home constructor.
     */
    public function __construct()
    {
        if(isset($_GET['area']) or isset($GET['region'])) {
            $this->area = $_GET['area'];
        } else if (isset($_GET['region'])) {
            $this->region = $_GET['region'];
        }
        $home = $this->model('Home');
        $this->view('home', [
            'contents' => $home->getContents($this->area, $this->region),
            'regions' => $home->getRegions(),
            'areas' => $home->getAreas()
        ]);
    }
}