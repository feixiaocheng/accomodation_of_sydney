<?php

use MarkWilson\XmlToJson\XmlToJsonConverter;

class Home
{
    /**
     * Home constructor.
     */
    function __construct()
    {
        $this->client = new GuzzleHttp\Client();
        $this->key = API_KEY;
    }

    /**
     * get the accommodation details in specific area or region
     * @param $area String
     * @param $region String
     * @return Arrary
     */
    function getContents($area = null, $region = null)
    {
        $converter = new XmlToJsonConverter();

        try {
            $url = "https://atlas.atdw-online.com.au/api/atlas/products?key={$this->key}";
            if ($area) {
                $url = $url . "&ar={$area}";
            }
            if ($region) {
                $url = $url . "&rg={$region}";
            }

            $res = $this->client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            die('Fail to connect to API');
        }

        $xml = $res->getBody();

        $xml = new \SimpleXMLElement($xml);
        $jsonRes = $converter->convert($xml);

        $resArray =  json_decode($jsonRes, true);
        $records = $resArray['atdw_search_response']['products']["product_record"];
        $contents = [];
        foreach ($records as $record) {
            $contents[] = [
                'owning_organisation_name' => $record['owning_organisation_name'],
                'product_name' => $record['product_name'],
                'product_description' => $record['product_description'],
                'product_image' => $record['product_image'],
                'address' => $record['addresses']['address']
            ];
        }

        return $contents;
    }

    /**
     * Get all the regions
     *
     * @return Arrary
     */
    function getRegions()
    {

        $converter = new XmlToJsonConverter();

        try {
            $url = "https://atlas.atdw-online.com.au/api/atlas/regions?key={$this->key}&st=NSW";
            $res = $this->client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            die('Fail to connect to API');
        }

        $xml = $res->getBody();

        $xml = new \SimpleXMLElement($xml);
        $jsonRes = $converter->convert($xml);

        $resArray =  json_decode($jsonRes, true);

        return $resArray['Regions']['Region'];
    }

    /**
     * Get all the areas
     *
     * @return Arrary
     */
    function getAreas()
    {
        $converter = new XmlToJsonConverter();

        try {
            $url = "https://atlas.atdw-online.com.au/api/atlas/areas?key={$this->key}";
            $res = $this->client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            die('Fail to connect to API');
        }

        $xml = $res->getBody();

        $xml = new \SimpleXMLElement($xml);
        $jsonRes = $converter->convert($xml);

        $resArray =  json_decode($jsonRes, true);
        return $resArray['Area']['Area'];
    }
}