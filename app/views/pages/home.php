<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sydney Accommodation</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/app/css/home.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
    <div class="col-md-12 center-block" id="container" >
        <div id="app">

            <div class="dropdown-field" style="display: inline-block">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Area
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php foreach ($areas as $area) {
                        ?>
                        <a href="?area=<?php echo $area['Name']; ?>"><li class="area" ><?php echo $area['Name']; ?></li></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="dropdown-field" style="display: inline-block">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Region
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php foreach ($regions as $region) {
                        ?>
                        <a href="?region=<?php echo $region['Name']; ?>"><li class="region"><?php echo $region['Name']; ?></li></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="accommodation-field">
                <?php foreach ($contents as $content) {
                    ?>
                    <div class="accommodation-box">
                        <img src=<?php echo $content['product_image']; ?>  alt=<?php echo $content['product_name']; ?> />
                        <p><?php echo $content['product_name']; ?></p>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</body>
</html>